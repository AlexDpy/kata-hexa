<?php

namespace Tests\Kata\Infrastructure;

use Kata\Domain\Invoice;
use Kata\Infrastructure\CSVInvoiceReader;
use Kata\Infrastructure\CSVInvoiceRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class CSVInvoiceRepositoryTest extends TestCase
{
    /**
     * @var CSVInvoiceRepository
     */
    private $csvInvoiceRepository;

    /**
     * @var CSVInvoiceReader|ObjectProphecy
     */
    private $csvInvoiceReader;

    protected function setUp()
    {
        $this->csvInvoiceReader = $this->prophesize(CSVInvoiceReader::class);

        $this->csvInvoiceRepository = new CSVInvoiceRepository(
            $this->csvInvoiceReader->reveal()
        );
    }

    /**
     * @test
     */
    public function itShould()
    {
        $this->csvInvoiceReader->getData(Argument::any())->willReturn([
            [
                'D67I76ZC',
                '540080',
                'D22M09Y2016',
                'D12M08Y2017',
                'marius',
                'back',
                'marius.back.pro@foobar.com',
            ],
        ]);

        $this->assertEquals(
            [
                new Invoice(
                    'D67I76ZC',
                    540080,
                    new \DateTimeImmutable('2016-09-22'),
                    new \DateTimeImmutable('2017-08-12'),
                    'marius',
                    'back',
                    'marius.back.pro@foobar.com'
                ),
            ],
            $this->csvInvoiceRepository->loadInvoices()
        );
    }
}
