<?php

namespace Tests\Kata\Domain\Rules;

use Kata\Domain\Clock;
use Kata\Domain\Invoice;
use Kata\Domain\Rules\PaymentReminder;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class PaymentReminderTest extends TestCase
{
    /**
     * @var PaymentReminder
     */
    private $paymentReminder;

    /**
     * @var Clock|ObjectProphecy
     */
    private $clock;

    protected function setUp()
    {
        $this->clock = $this->prophesize(Clock::class);

        $this->paymentReminder = new PaymentReminder(
            $this->clock->reveal()
        );
    }

    /**
     * @test
     */
    public function itShouldMatch()
    {
        $now = new \DateTimeImmutable('2017-01-01');
        $this->clock->now()->willReturn($now);
        $dueDate = new \DateTimeImmutable('2016-12-01');

        $invoice = new Invoice(
            'ref',
            123,
            $dueDate,
            null,
            'Doe',
            'John',
            'john@doe.com'
        );

        $this->assertTrue($this->paymentReminder->match($invoice));
    }

    /**
     * @test
     */
    public function itShouldMatchForTheSpecialRule()
    {
        $now = new \DateTimeImmutable('2017-09-30');
        $this->clock->now()->willReturn($now);
        $dueDate = new \DateTimeImmutable('2017-08-31');

        $invoice = new Invoice(
            'ref',
            123,
            $dueDate,
            null,
            'Doe',
            'John',
            'john@doe.com'
        );

        $this->assertTrue($this->paymentReminder->match($invoice));
    }
}
