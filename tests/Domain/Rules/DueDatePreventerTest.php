<?php

namespace Tests\Kata\Domain\Rules;

use Kata\Domain\Clock;
use Kata\Domain\Invoice;
use Kata\Domain\Mails\DueDatePreventerMailer;
use Kata\Domain\Rules\DueDatePreventer;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class DueDatePreventerTest extends TestCase
{
    /**
     * @var DueDatePreventer
     */
    private $dueDatePreventer;

    /**
     * @var Clock|ObjectProphecy
     */
    private $clock;

    /**
     * @var DueDatePreventerMailer|ObjectProphecy
     */
    private $dueDatePreventerMailer;

    protected function setUp()
    {
        $this->clock = $this->prophesize(Clock::class);
        $this->dueDatePreventerMailer = $this->prophesize(DueDatePreventerMailer::class);

        $this->dueDatePreventer = new DueDatePreventer(
            $this->clock->reveal(),
            $this->dueDatePreventerMailer->reveal()
        );
    }

    /**
     * @test
     * @dataProvider dueDates
     */
    public function itShouldMatch10DaysBeforeDueDate(\DateTimeImmutable $dueDate)
    {
        $invoice = new Invoice(
            'ref',
            123,
            $dueDate,
            null,
            'Doe',
            'John',
            'john@doe.com'
        );


        $now = new \DateTimeImmutable('2017-01-05');
        $this->clock->now()->willReturn($now);

        if ($dueDate->format('d') === '15') {
            $this->assertEquals(true, $this->dueDatePreventer->match($invoice));
        } else {
            $this->assertEquals(false, $this->dueDatePreventer->match($invoice));
        }
    }

    /**
     * @test
     */
    public function itShouldSendTheMail()
    {
        $invoice = new Invoice(
            'ref',
            123,
            new \DateTimeImmutable('2017-01-05'),
            null,
            'Doe',
            'John',
            'john@doe.com'
        );

        $this->dueDatePreventerMailer->sendMail($invoice)->shouldBeCalled(1);

        $this->dueDatePreventer->apply($invoice);
    }

    /**
     * @return \DateTimeImmutable[]
     */
    public function dueDates()
    {
        return [
            [new \DateTimeImmutable('2017-01-01')],
            [new \DateTimeImmutable('2017-01-02')],
            [new \DateTimeImmutable('2017-01-03')],
            [new \DateTimeImmutable('2017-01-04')],
            [new \DateTimeImmutable('2017-01-05')],
            [new \DateTimeImmutable('2017-01-06')],
            [new \DateTimeImmutable('2017-01-07')],
            [new \DateTimeImmutable('2017-01-08')],
            [new \DateTimeImmutable('2017-01-09')],
            [new \DateTimeImmutable('2017-01-10')],
            [new \DateTimeImmutable('2017-01-11')],
            [new \DateTimeImmutable('2017-01-12')],
            [new \DateTimeImmutable('2017-01-13')],
            [new \DateTimeImmutable('2017-01-14')],
            [new \DateTimeImmutable('2017-01-15')],
            [new \DateTimeImmutable('2017-01-16')],
            [new \DateTimeImmutable('2017-01-17')],
            [new \DateTimeImmutable('2017-01-18')],
            [new \DateTimeImmutable('2017-01-19')],
            [new \DateTimeImmutable('2017-01-20')],
            [new \DateTimeImmutable('2017-01-21')],
            [new \DateTimeImmutable('2017-01-22')],
            [new \DateTimeImmutable('2017-01-23')],
            [new \DateTimeImmutable('2017-01-24')],
            [new \DateTimeImmutable('2017-01-25')],
            [new \DateTimeImmutable('2017-01-26')],
            [new \DateTimeImmutable('2017-01-27')],
            [new \DateTimeImmutable('2017-01-28')],
            [new \DateTimeImmutable('2017-01-29')],
            [new \DateTimeImmutable('2017-01-30')],
        ];
    }
}
