# Kata PHP Starter

## Create the project

```bash
composer create-project -s dev alexdpy/kata-php-starter TheSuperKata && cd TheSuperKata
```

## Go!

To run your phpunit tests:
```bash
make test
```

To open the generated html code coverage with firefox
```bash
make coverage
```

To run the `kata.php` script:
```bash
make run
```
