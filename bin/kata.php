<?php

require_once __DIR__ . '/../vendor/autoload.php';

$invoiceRepository = new \Kata\Infrastructure\CSVInvoiceRepository(
    new \Kata\Infrastructure\CSVInvoiceReader()
);

$clock = new \Kata\Domain\Clock();

/** @var \Kata\Domain\Rules\Rule[] $rules */
$rules = [
    new \Kata\Domain\Rules\DueDatePreventer($clock),
    new \Kata\Domain\Rules\PaymentReminder($clock),
];

$invoices = $invoiceRepository->loadInvoices();

foreach ($invoices as $invoice) {
    foreach ($rules as $rule) {
        if ($rule->match($invoice)) {
            $rule->apply($invoice);
        }
    }
}

