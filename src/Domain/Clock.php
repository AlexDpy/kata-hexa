<?php

namespace Kata\Domain;

class Clock
{
    /**
     * @return \DateTimeImmutable
     */
    public function now()
    {
        return new \DateTimeImmutable('2017-02-02');
    }
}
