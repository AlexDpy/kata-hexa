<?php

namespace Kata\Domain;

class Invoice
{
    /**
     * @var string
     */
    private $reference;

    /**
     * @var int
     */
    private $price;

    /**
     * @var \DateTimeImmutable
     */
    private $dueDate;

    /**
     * @var \DateTimeImmutable
     */
    private $paymentDate;

    /**
     * @var string
     */
    private $contactLastName;

    /**
     * @var string
     */
    private $contactFirstName;

    /**
     * @var string
     */
    private $contactEmail;

    /**
     * Invoice constructor.
     *
     * @param string $reference
     * @param int $price
     * @param \DateTimeImmutable $dueDate
     * @param \DateTimeImmutable $paymentDate
     * @param string $contactLastName
     * @param string $contactFirstName
     * @param string $contactEmail
     */
    public function __construct(
        $reference,
        $price,
        \DateTimeImmutable $dueDate,
        \DateTimeImmutable $paymentDate = null,
        $contactLastName,
        $contactFirstName,
        $contactEmail
    ) {
        $this->reference = $reference;
        $this->price = $price;
        $this->dueDate = $dueDate;
        $this->paymentDate = $paymentDate;
        $this->contactLastName = $contactLastName;
        $this->contactFirstName = $contactFirstName;
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @return string
     */
    public function getContactLastName()
    {
        return $this->contactLastName;
    }

    /**
     * @return string
     */
    public function getContactFirstName()
    {
        return $this->contactFirstName;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return null !== $this->paymentDate;
    }

    /**
     * @return string
     */
    public function getDisplayablePrice()
    {
        $string = (string) $this->price;

        return substr($string, 0, -2) . '.' . substr($string, -2) . '€';
    }
}
