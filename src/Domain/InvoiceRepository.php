<?php

namespace Kata\Domain;

interface InvoiceRepository
{
    /**
     * @return Invoice[]
     */
    public function loadInvoices();
}
