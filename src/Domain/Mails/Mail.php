<?php

namespace Kata\Domain\Mails;

class Mail
{
    /**
     * @var string
     */
    private $recipient;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $text;

    /**
     * @param string $recipient
     * @param string $subject
     * @param string $text
     */
    public function __construct(
        $recipient,
        $subject,
        $text
    )
    {
        $this->recipient = $recipient;
        $this->subject = $subject;
        $this->text = $text;
    }
}
