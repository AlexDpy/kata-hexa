<?php

namespace Kata\Domain\Mails;

use Kata\Domain\Invoice;

class DueDatePreventerMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMail(Invoice $invoice)
    {
        $mail = new Mail(
            $invoice->getContactEmail(),
            $this->getSubject($invoice),
            $this->getText($invoice)
        );

        $this->mailer->send($mail);
    }

    /**
     * @param Invoice $invoice
     *
     * @return string
     */
    private function getSubject(Invoice $invoice)
    {
        return 'Votre Facture n° ' . $invoice->getReference();
    }

    /**
     * @param Invoice $invoice
     *
     * @return string
     */
    private function getText(Invoice $invoice)
    {
        $text = <<<EOT
Bonjour {$invoice->getContactLastName()} {$invoice->getContactFirstName()},

Nous attendons votre réglement de {$invoice->getDisplayablePrice()} pour le {$invoice->getDueDate()->format('d/m/Y')}

Cordialement,
Le Service Facturation
EOT;

        return $text;
    }
}
