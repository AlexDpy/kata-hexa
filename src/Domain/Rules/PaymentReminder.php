<?php

namespace Kata\Domain\Rules;

use Kata\Domain\Clock;
use Kata\Domain\Invoice;

class PaymentReminder implements Rule
{
    /**
     * @var Clock
     */
    private $clock;

    /**
     * @param Clock $clock
     */
    public function __construct(Clock $clock)
    {
        $this->clock = $clock;
    }

    public function match(Invoice $invoice)
    {
        $dueDate = $invoice->getDueDate();
        $now = $this->clock->now();

        if ($dueDate < $now && !$invoice->isPaid()) {
            $next = $this->getNextDate($dueDate);

            return $next == $now;
        }

        return false;
    }

    public function apply(Invoice $invoice)
    {

        echo $invoice->getContactEmail() . PHP_EOL;
    }

    /**
     * @param $dueDate
     * @return mixed
     */
    private function getNextDate(\DateTimeImmutable $dueDate)
    {
        $next = $dueDate->add(new \DateInterval('P1M'));

        if ($next->format('d') != $dueDate->format('d')) {
            // retour au dernier jour du mois precedant
            $next = $next->sub(new \DateInterval('P' . $next->format('d') . 'D'));

            return $next;
        }

        return $next;
    }
}
