<?php

namespace Kata\Domain\Rules;

use Kata\Domain\Invoice;

interface Rule
{
    /**
     * @param Invoice $invoice
     * @return bool
     */
    public function match(Invoice $invoice);

    /**
     * @param Invoice $invoice
     */
    public function apply(Invoice $invoice);
}
