<?php

namespace Kata\Domain\Rules;

use Kata\Domain\Clock;
use Kata\Domain\Invoice;
use Kata\Domain\Mails\DueDatePreventerMailer;

class DueDatePreventer implements Rule
{
    /**
     * @var Clock
     */
    private $clock;

    /**
     * @var DueDatePreventerMailer
     */
    private $dueDatePreventerMailer;

    /**
     * @param Clock                  $clock
     * @param DueDatePreventerMailer $dueDatePreventerMailer
     */
    public function __construct(Clock $clock, DueDatePreventerMailer $dueDatePreventerMailer)
    {
        $this->clock = $clock;
        $this->dueDatePreventerMailer = $dueDatePreventerMailer;
    }

    public function match(Invoice $invoice)
    {
        $now = $this->clock->now();
        $dueDate = $invoice->getDueDate();

        $d2 = $now->add(new \DateInterval('P10D'));

        return $d2 == $dueDate;
    }

    public function apply(Invoice $invoice)
    {
        $this->dueDatePreventerMailer->sendMail($invoice);
    }
}
