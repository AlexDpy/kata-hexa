<?php

namespace Kata\Infrastructure;

use Kata\Domain\Invoice;
use Kata\Domain\InvoiceRepository;

class CSVInvoiceRepository implements InvoiceRepository
{
    /**
     * @var CSVInvoiceReader
     */
    private $csvInvoiceReader;

    /**
     * @param CSVInvoiceReader $csvInvoiceReader
     */
    public function __construct(CSVInvoiceReader $csvInvoiceReader)
    {
        $this->csvInvoiceReader = $csvInvoiceReader;
    }

    /**
     * @return Invoice[]
     */
    public function loadInvoices()
    {
        $invoices = [];

        foreach ($this->csvInvoiceReader->getData(__DIR__ . '/../../invoices.csv') as $data) {
            $invoices[] = new Invoice(
                $data[0],
                (int) $data[1],
                empty($data[2]) ? null : $this->decodeDate($data[2]),
                empty($data[3]) ? null : $this->decodeDate($data[3]),
                $data[4],
                $data[5],
                $data[6]
            );
        }

        return $invoices;
    }

    /**
     * @param string $csvDate
     *
     * @return \DateTimeImmutable
     */
    private function decodeDate($csvDate)
    {
        preg_match('/D(\d+)M(\d+)Y(\d+)/', $csvDate, $matches);

        return new \DateTimeImmutable(sprintf('%s-%s-%s', $matches[3], $matches[2], $matches[1]));
    }

}
