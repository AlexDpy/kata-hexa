<?php

namespace Kata\Infrastructure;

class CSVInvoiceReader
{
    /**
     * @param string $filename
     * @return array
     */
    public function getData($filename)
    {
        $data = [];

        $row = 0;
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($d = fgetcsv($handle, 0, ',')) !== false) {
                if ($row >= 1) {
                    $data[] = $d;
                }

                ++$row;
            }
            fclose($handle);
        }

        return $data;
    }
}
